# README #

1/3スケールのSEGA SC-3000H風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- SEGA

## 発売時期
- 1984年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/SC-3000)
- [セガハード大百科](https://sega.jp/history/hard/sc3000/index.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_sc-3000h/raw/357857bf4b01f911e93d2fce0fd8ea433cf3cc7b/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sc-3000h/raw/357857bf4b01f911e93d2fce0fd8ea433cf3cc7b/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_sc-3000h/raw/357857bf4b01f911e93d2fce0fd8ea433cf3cc7b/ExampleImage.png)
